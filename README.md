# Description

Auto1 Test Task

Demo: [http://auto1.dendavidov.com](http://auto1.dendavidov.com)

# To start

`yarn` or `npm install`


`yarn start` or `npm start`

#Features to consider
- middleware for API
- redux-form with meta-definitions
- table-view with meta-description

All these features have been implemented to allow adding new instances (for example new inputs or table columns) really fast.



# Server mock
Server side with API is implemented in the project 
https://github.com/dendavidov/auto1-server-mock

url: [http://89.223.30.138:1337/api/v1/merchants](http://89.223.30.138:1337/api/v1/merchants)

# How to reset database

To reset database go to url [http://localhost:3000/reset](http://localhost:3000/reset) and click the button

