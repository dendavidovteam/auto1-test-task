import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import TableView from '../../components/table-view/TableView';
import Checkbox from '../../components/checkbox/Checkbox';

import { actions } from '../../redux/merchants';

import styles from './styles.styl';

const map = [
  {
    name: 'firstname',
    title: 'First name',
  },
  {
    name: 'lastname',
    title: 'Last name',
  },
  {
    name: 'email',
    title: 'Email',
  },
  {
    name: 'phone',
    title: 'Phone',
  },
  {
    name: 'hasPremium',
    title: 'Premium',
    component: Checkbox,
  },
];

class MerchantsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getList();
  }

  handleDelete = items => {
    this.props.deleteItems(items, this.props.getList);
  };

  render() {
    return (
      <div className={styles.root}>
        <TableView
          items={this.props.merchantsList}
          useStatusProcessor
          map={map}
          refresh={this.props.getList}
          delete={this.handleDelete}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  merchantsList: state.merchants.list,
});

const mapActionsToProps = dispatch =>
  bindActionCreators(
    {
      getList: actions.getList,
      deleteItems: actions.deleteItems,
    },
    dispatch
  );

MerchantsList.propTypes = {
  merchantsList: PropTypes.shape().isRequired,
  getList: PropTypes.func.isRequired,
  deleteItems: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(MerchantsList);
