import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { resetDb } from '../../redux/dbfiller';
import { REQUEST_STATUSES } from '../../libs/api/constants';

class ResetDb extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  get status() {
    switch (this.props.status) {
      case REQUEST_STATUSES.REQUEST:
        return 'Loading...';
      case REQUEST_STATUSES.ERROR:
        return 'Error';
      case REQUEST_STATUSES.SUCCESS:
        return 'Success';
      default:
        return null;
    }
  }

  render() {
    return (
      <div>
        <p>{this.status}</p>
        <input
          type="button"
          className="btn btn-primary"
          disabled={this.props.status === REQUEST_STATUSES.REQUEST}
          value="RESET DB"
          onClick={this.props.resetDb}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  status: state.dbfiller.status,
});

const mapActionsToProps = dispatch =>
  bindActionCreators(
    {
      resetDb,
    },
    dispatch
  );

ResetDb.propTypes = {
  status: PropTypes.string.isRequired,
  resetDb: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(ResetDb);
