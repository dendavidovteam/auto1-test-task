import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../redux/merchants';

import createForm from '../../components/forms/form/Form';
import { FORM_TYPES } from '../../components/forms/form-group/FormGroup';
import BidsList from '../../components/bids-list/BidsList';

export const formData = {
  name: 'merchant',
  submitButton: 'Save',
  fields: [
    {
      name: 'firstname',
      label: 'First Name',
      type: FORM_TYPES.TEXT,
      required: true,
    },
    {
      name: 'lastname',
      label: 'Last Name',
      type: FORM_TYPES.TEXT,
      required: true,
    },
    {
      name: 'avatarUrl',
      label: 'Avatar',
      type: FORM_TYPES.PICTURE,
    },
    {
      name: 'email',
      label: 'Email',
      type: FORM_TYPES.TEXT,
      required: true,
      reg: 'email',
    },
    {
      name: 'phone',
      label: 'Phone',
      type: FORM_TYPES.TEXT,
      required: true,
    },
    {
      name: 'hasPremium',
      label: 'Premium',
      type: FORM_TYPES.CHECKBOX,
    },
    {
      name: 'bids',
      label: 'Bids',
      type: FORM_TYPES.CUSTOM,
      component: BidsList,
    },
  ],
};

class Merchant extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    if (!this.isAdd) {
      this.props.fetchMerchant(this.props.match.params.id);
    }
  }

  get Form() {
    return createForm(formData.name, {
      initialValues: this.isAdd
        ? { bids: [] }
        : { ...this.props.merchantInfo.data },
    });
  }

  get isAdd() {
    return this.props.match.params.id === 'new';
  }

  handleSubmit = values => {
    const { id } = this.props.match.params;
    if (id && id !== 'new') {
      this.props.editMerchant(id, values, this.props.history.goBack);
    } else {
      this.props.addMerchant(values, this.props.history.goBack);
    }
  };

  handleCancel = () => {
    this.props.history.goBack();
  };

  render() {
    return (
      <div>
        <h1>{this.isAdd ? 'Add merchant' : 'Edit merchant'}</h1>
        <this.Form
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          formData={formData}
          items={this.isAdd ? this.props.merchantAdd : this.props.merchantEdit}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  merchantInfo: state.merchants.item,
  merchantEdit: state.merchants.edit,
  merchantAdd: state.merchants.add,
});

const mapActionsToProps = dispatch =>
  bindActionCreators(
    {
      fetchMerchant: actions.getItem,
      addMerchant: actions.addItem,
      editMerchant: actions.editItem,
    },
    dispatch
  );

Merchant.propTypes = {
  match: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  merchantInfo: PropTypes.shape().isRequired,
  merchantEdit: PropTypes.shape().isRequired,
  merchantAdd: PropTypes.shape().isRequired,
  fetchMerchant: PropTypes.func.isRequired,
  addMerchant: PropTypes.func.isRequired,
  editMerchant: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Merchant);
