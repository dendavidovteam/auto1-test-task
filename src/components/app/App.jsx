import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import logo from './logo.png';
import './App.css';

import MerchantsList from '../../pages/merchants-list/MerchantsList';
import Merchant from '../../pages/merchant/Merchant';
import ResetDb from '../../pages/resetdb/ResetDb';

class App extends Component {
  state = {};

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Link to="/">
            <img src={logo} className="App-logo" alt="logo" />
          </Link>
          <h2>Welcome</h2>
        </div>
        <div className="App-container">
          <Switch>
            <Route path="/" exact component={MerchantsList} />
            <Route path="/reset" exact component={ResetDb} />
            <Route path="/:id" exact component={Merchant} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
