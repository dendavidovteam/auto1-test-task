import { paginate } from './paginate';

describe('paginate', () => {
  it('returns [(1) 2 3 > >>] for totalPages = 10 and currentPos=1', () => {
    expect(paginate(1, 10)).toEqual([1, 2, 3, '>', '>>']);
  });

  it('returns [< 1 (2) 3 4 > >>] for totalPages = 10 and currentPos=2', () => {
    expect(paginate(2, 10)).toEqual(['<', 1, 2, 3, 4, '>', '>>']);
  });

  it('returns [<< < 3 4 (5) 6 7 > >>] for totalPages = 10 and currentPos=5', () => {
    expect(paginate(5, 10)).toEqual(['<<', '<', 3, 4, 5, 6, 7, '>', '>>']);
  });

  it('returns [<< < 7 8 (9) 10 >] for totalPages = 10 and currentPos=9', () => {
    expect(paginate(9, 10)).toEqual(['<<', '<', 7, 8, 9, 10, '>']);
  });

  it('returns [1 2 3] for totalPages = 3 and currentPos=2', () => {
    expect(paginate(2, 3)).toEqual([1, 2, 3]);
  });
});
