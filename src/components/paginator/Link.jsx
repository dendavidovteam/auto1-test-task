import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link as NativeLink } from 'react-router-dom';
import classNames from 'classnames';

const Link = props => (
  <li className={classNames('page-item', { active: props.isActive })}>
    <NativeLink
      className="page-link"
      to={`${props.location.pathname}?page=${props.page}`}
    >
      {props.text}
    </NativeLink>
  </li>
);

Link.propTypes = {
  location: PropTypes.shape().isRequired,
  text: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired,
  ]).isRequired,
  page: PropTypes.number.isRequired,
  isActive: PropTypes.bool,
};

Link.defaultProps = {
  isActive: false,
};

export default withRouter(Link);
