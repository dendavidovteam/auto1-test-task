export const foo = () => {};

export const paginate = (currentPage, totalPages) => {
  const result = [];
  if (totalPages <= 3) return [1, 2, 3];
  if (currentPage >= 3) result.push('<<');
  if (currentPage >= 2) result.push('<');
  if (currentPage === 2) result.push(1);
  if (currentPage >= 3) result.push(currentPage - 2, currentPage - 1);
  result.push(currentPage);
  const diff = totalPages - currentPage;
  if (diff >= 2) result.push(currentPage + 1, currentPage + 2, '>', '>>');
  if (diff === 1) result.push(currentPage + 1, '>');

  return result;
};
