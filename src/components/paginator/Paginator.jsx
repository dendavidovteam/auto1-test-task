import React from 'react';
import PropTypes from 'prop-types';

import Link from './Link';

import { paginate } from './paginate';

const Paginator = props => {
  const items = paginate(props.currentPage + 1, props.totalPages);

  return (
    <ul className="pagination">
      {items.map(item => {
        if (item === '<<') return <Link key={item} page={1} text={'<<'} />;
        if (item === '<')
          return <Link key={item} page={props.currentPage} text={'<'} />;
        if (item === '>')
          return <Link key={item} page={props.currentPage + 2} text={'>'} />;
        if (item === '>>')
          return <Link key={item} page={props.totalPages} text={'>>'} />;
        return (
          <Link
            key={item}
            page={item}
            text={item}
            isActive={item === props.currentPage + 1}
          />
        );
      })}
    </ul>
  );
};

Paginator.propTypes = {
  currentPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
};

export default Paginator;
