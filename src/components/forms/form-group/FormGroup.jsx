import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import ImgUploader from '../img-uploader/ImgUploader';

import styles from './styles.styl';

export const FORM_TYPES = {
  TEXT: 'text',
  NUMBER: 'number',
  TEXTAREA: 'textarea',
  CHECKBOX: 'checkbox',
  PICTURE: 'picture',
  CUSTOM: 'custom',
};

class FormGroup extends React.Component {
  get text() {
    const {
      input,
      label,
      maxLength,
      id,
      type,
      readOnly,
      meta: { error, warning },
    } = this.props;
    return (
      <input
        className={classNames('form-control', {
          'is-invalid': error || warning,
        })}
        id={id}
        {...input}
        placeholder={label}
        type={type}
        maxLength={maxLength}
        readOnly={readOnly}
      />
    );
  }

  get checkbox() {
    return (
      <input
        className={styles.checkbox}
        type="checkbox"
        {...this.props.input}
      />
    );
  }

  get picture() {
    return <ImgUploader input={this.props.input} />;
  }

  get input() {
    const { type } = this.props;
    switch (type) {
      case FORM_TYPES.TEXT:
      case FORM_TYPES.NUMBER:
        return this.text;
      case FORM_TYPES.TEXTAREA:
        return this.textarea;
      case FORM_TYPES.CHECKBOX:
        return this.checkbox;
      case FORM_TYPES.PICTURE:
        return this.picture;
      case FORM_TYPES.CUSTOM:
        return this.customComponent;
      default:
        return this.text;
    }
  }

  get customComponent() {
    return (
      <this.props.customComponent
        value={this.props.input.value}
        onChange={this.props.input.onChange}
      />
    );
  }

  get label() {
    const { id, label, required } = this.props;
    return (
      <label className="control-label" htmlFor={id}>
        {label}
        {required && ' *'}
      </label>
    );
  }

  get validation() {
    const {
      meta: { touched, error, warning },
    } = this.props;
    if (!touched) return null;
    if (error) return <div className="invalid-feedback">{error}</div>;
    if (warning) return <div className="invalid-feedback">{warning}</div>;
    return null;
  }

  render() {
    return (
      <div className="form-group has-danger">
        <fieldset>
          {this.label}
          {this.input}
          {this.validation}
        </fieldset>
      </div>
    );
  }
}

FormGroup.propTypes = {
  input: PropTypes.shape().isRequired,
  label: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.shape().isRequired,
  required: PropTypes.bool,
  readOnly: PropTypes.bool,
  maxLength: PropTypes.number,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape()),
    PropTypes.func,
  ]),
  dynamicData: PropTypes.shape(),
};

FormGroup.defaultProps = {
  required: false,
  maxLength: null,
  options: null,
  dynamicData: null,
  readOnly: false,
};

export default FormGroup;
