import React from 'react';
import PropTypes from 'prop-types';

import './file-upload-styles.css';

class ImgUploader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false };
  }

  componentDidMount() {
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({ loaded: true });
    // eslint-disable-next-line global-require
    this.ImagesUploader = require('react-images-uploader').default;
  }

  onLoadEnd = (err, response) => {
    if (Array.isArray(response) && response[0]) {
      this.props.input.onChange(response[0]);
    }
  };

  render() {
    if (!this.state.loaded) return null;

    const { ImagesUploader } = this;

    return (
      <div>
        <ImagesUploader
          image={this.props.input.value}
          url={`${process.env.API_URL}files`}
          optimisticPreviews
          multiple={false}
          onLoadEnd={this.onLoadEnd}
        />
      </div>
    );
  }
}

ImgUploader.propTypes = {
  input: PropTypes.shape().isRequired,
};

export default ImgUploader;
