import { reduxForm } from 'redux-form';

import FormRaw from './FormRaw';

const regs = {
  email: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
};

const validate = (values, { formData }) => {
  const errors = {};
  if (!formData || !Array.isArray(formData.fields)) return errors;

  formData.fields.forEach(field => {
    const value = values[field.name];

    if (!!field.required && !value) {
      errors[field.name] = 'Required field';
      return;
    }
    if (!!field.reg && !!value && !regs[field.reg].test(value)) {
      errors[field.name] = 'Wrong format';
    }
  });
  return errors;
};

const createForm = (name, params = {}) =>
  reduxForm({
    form: name,
    validate,
    ...params,
  })(FormRaw);

export default createForm;
