import React from 'react';
import PropTypes from 'prop-types';
import MDSpinner from 'react-md-spinner';
import { Field } from 'redux-form';
import classNames from 'classnames';

import FormGroup from '../form-group/FormGroup';
import { REQUEST_STATUSES } from '../../../libs/api/constants';

import styles from './styles.styl';

class FormRaw extends React.Component {
  componentWillUnmount() {
    if (typeof this.props.clear === 'function') this.props.clear();
  }

  get renderedForm() {
    const { handleSubmit, onCancel, formData } = this.props;
    return (
      <form
        onSubmit={handleSubmit}
        className={classNames({ [styles.disabled]: this.isLoading })}
      >
        {formData &&
          Array.isArray(formData.fields) &&
          formData.fields &&
          Array.isArray(formData.fields) &&
          formData.fields.map(field => (
            <Field
              key={`${formData.name}.${field.name}`}
              id={`${formData.name}.${field.name}`}
              name={field.name}
              label={field.label}
              component={FormGroup}
              type={field.type}
              required={!!field.required}
              customComponent={field.component}
              readOnly={formData.readOnly}
            />
          ))}
        <div className={styles.buttons}>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={this.isLoading}
          >
            Save
          </button>
          {typeof onCancel === 'function' && (
            <button
              type="button"
              className="btn btn-primary"
              disabled={this.isLoading}
              onClick={onCancel}
            >
              Cancel
            </button>
          )}
        </div>
      </form>
    );
  }

  get isLoading() {
    const { status } = this.props.items;
    return status === REQUEST_STATUSES.REQUEST;
  }

  get isSuccess() {
    const { status } = this.props.items;
    return status === REQUEST_STATUSES.SUCCESS;
  }

  get error() {
    const { status, errorMessage } = this.props.items;
    return status === REQUEST_STATUSES.ERROR ? errorMessage || 'Error' : null;
  }

  get statusBar() {
    if (this.error)
      return (
        <div className="alert alert-dismissible alert-warning">
          <button type="button" className="close" onClick={this.props.clear}>
            &times;
          </button>
          <h4 className="alert-heading">Error!</h4>
          <p className="mb-0">{this.error}</p>
        </div>
      );
    if (this.isSuccess && this.props.formData.successMessage)
      return (
        <div className="alert alert-dismissible alert-warning">
          <button type="button" className="close" onClick={this.props.clear}>
            &times;
          </button>
          <h4 className="alert-heading">
            {this.props.formData.successMessage}
          </h4>
        </div>
      );
    return null;
  }

  get loader() {
    const size = 36;
    return (
      this.isLoading && (
        <div className={styles.loader}>
          <div
            className={styles.wrapper}
            style={{
              marginTop: -size / 2,
              width: size,
            }}
          >
            <MDSpinner size={size} />
          </div>
        </div>
      )
    );
  }

  render() {
    return (
      <div className={styles.root}>
        {this.statusBar}
        {this.renderedForm}
        {this.loader}
      </div>
    );
  }
}

FormRaw.propTypes = {
  handleSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  clear: PropTypes.func,
  formData: PropTypes.shape().isRequired,
  items: PropTypes.shape(),
};

FormRaw.defaultProps = {
  handleSubmit: null,
  onCancel: null,
  clear: null,
  items: {},
};

export default FormRaw;
