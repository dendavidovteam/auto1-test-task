import React from 'react';
import { shallow } from 'enzyme';

import DataStatusProcessor from './DataStatusProcessor';
import { REQUEST_STATUSES } from '../../libs/api/constants';

describe('DataStatusProcessor', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <DataStatusProcessor
        items={{
          status: REQUEST_STATUSES.ERROR,
          errorMessage: 'test',
        }}
      />
    );
  });

  it('should render', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render div', () => {
    expect(wrapper.is('div')).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
