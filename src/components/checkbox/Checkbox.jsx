import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <input type="checkbox" checked={this.props.value} readOnly />;
  }
}

Checkbox.propTypes = {
  value: PropTypes.bool,
};

Checkbox.defaultProps = {
  value: false,
};
export default Checkbox;
