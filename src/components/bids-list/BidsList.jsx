import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import TableView from '../table-view/TableView';

const map = [
  {
    name: 'carTitle',
    title: 'Car Title',
  },
  {
    name: 'amount',
    title: 'Amount',
  },

  {
    name: 'created',
    title: 'Created',
    // eslint-disable-next-line react/prop-types
    component: ({ value }) => <span>{moment(value).format('DD.MM.YYYY')}</span>,
  },
];

const BidsList = props => {
  const { value } = props;

  const sortedValues = value.sort(
    (a, b) => new Date(a.created) - new Date(b.created)
  );
  return sortedValues.length > 0 ? (
    <TableView itemsPerPage={10000} readOnly items={sortedValues} map={map} />
  ) : (
    <p>No items</p>
  );
};

BidsList.propTypes = {
  value: PropTypes.arrayOf(PropTypes.shape({})),
};

BidsList.defaultProps = {
  value: null,
};

export default BidsList;
