import React from 'react';
import { shallow } from 'enzyme';

import BidsList from './BidsList';

describe('BidsList', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<BidsList value={[]} />);
  });

  it('should render', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render p', () => {
    expect(wrapper.is('p')).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
