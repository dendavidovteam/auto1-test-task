import React from 'react';
import { shallow } from 'enzyme';

import ErrorComponent from './ErrorComponent';

describe('ErrorComponent', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ErrorComponent error={{ message: 'Error' }} />);
  });

  it('should render', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render div', () => {
    expect(wrapper.is('div')).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
