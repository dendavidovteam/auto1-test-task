import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import DataStatusProcessor from '../data-status-processor/DataStatusProcessor';
import Paginator from '../paginator/Paginator';

import { getPageFromSearchParam } from '../../libs/search';

import styles from './styles.styl';

class TableView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: getPageFromSearchParam(props.location.search),
      selectedItems: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.search !== nextProps.location.search) {
      this.setState({
        currentPage: getPageFromSearchParam(nextProps.location.search),
      });
    }
  }

  get paginator() {
    const { items } = this;

    if (
      !items ||
      !Array.isArray(items) ||
      items.length <= this.props.itemsPerPage
    )
      return null;

    const totalPages = Math.ceil(items.length / this.props.itemsPerPage);

    return (
      <Paginator currentPage={this.state.currentPage} totalPages={totalPages} />
    );
  }

  get items() {
    const { useStatusProcessor, items } = this.props;
    return useStatusProcessor ? items.data : items;
  }

  get renderedItems() {
    const { itemsPerPage } = this.props;
    const { items } = this;

    if (!items || !Array.isArray(items)) return null;

    const visibleItems =
      items.length <= itemsPerPage
        ? items
        : items.slice(
            this.state.currentPage * itemsPerPage,
            (this.state.currentPage + 1) * itemsPerPage
          );

    return (
      <div className={styles.tableWrapper}>
        <table className="table">
          {this.getHeader()}
          <tbody>{this.getRows(visibleItems)}</tbody>
        </table>
      </div>
    );
  }

  getRows = items =>
    items.map(item => (
      <tr
        key={item.id}
        onClick={() => {
          if (!this.props.readOnly) this.props.history.push(`/${item.id}`);
        }}
      >
        {!this.props.readOnly && (
          <td>
            <input
              type="checkbox"
              checked={!!this.state.selectedItems[item.id]}
              onChange={() => this.toggleSelectedItem(item.id)}
              onClick={e => e.stopPropagation()}
            />
          </td>
        )}
        {this.props.map.map(mapItem => (
          <td className={styles.tableCell} key={mapItem.name}>
            {this.getCellContent(item, mapItem)}
          </td>
        ))}
      </tr>
    ));

  getHeader = () => (
    <thead>
      <tr>
        {!this.props.readOnly && (
          <th className={styles.headerCell}>
            <input
              type="checkbox"
              checked={this.areAllItemsChecked()}
              onChange={this.toggleAllItemsSelected}
            />
          </th>
        )}
        {this.props.map.map(mapItem => (
          <th className={styles.headerCell} key={mapItem.name}>
            {mapItem.title}
          </th>
        ))}
      </tr>
    </thead>
  );

  getCellContent = (item, mapItem) =>
    mapItem.component ? (
      <mapItem.component value={item[mapItem.name]} />
    ) : (
      item[mapItem.name]
    );

  areAllItemsChecked = () =>
    Object.keys(this.state.selectedItems).filter(
      key => this.state.selectedItems[key]
    ).length === this.items.length && this.items.length > 0;

  toggleSelectedItem = itemId => {
    this.setState({
      selectedItems: {
        ...this.state.selectedItems,
        [itemId]: !this.state.selectedItems[itemId],
      },
    });
  };

  toggleAllItemsSelected = () => {
    const selectedItems = {};
    if (!this.areAllItemsChecked()) {
      this.items.forEach(item => {
        selectedItems[item.id] = true;
      });
    }

    this.setState({
      selectedItems,
    });
  };

  delete = () => {
    const { selectedItems } = this.state;
    this.props.delete(
      Object.keys(selectedItems).filter(key => selectedItems[key])
    );
  };

  isDeleteDisabled = () => {
    const { selectedItems } = this.state;
    const filteredArray = Object.keys(selectedItems).filter(
      key => selectedItems[key]
    );
    return filteredArray.length === 0;
  };

  topButtons = () => (
    <div className={styles.topButtons}>
      <Link className="btn btn-primary" to={`${this.props.linkPrefix}/new`}>
        Add
      </Link>
      {typeof this.props.refresh === 'function' && (
        <button className="btn btn-primary" onClick={this.props.refresh}>
          Refresh
        </button>
      )}
      {typeof this.props.delete === 'function' && (
        <button
          className="btn btn-primary"
          onClick={this.delete}
          disabled={this.isDeleteDisabled()}
        >
          Delete
        </button>
      )}
    </div>
  );

  render() {
    const { useStatusProcessor, items, readOnly } = this.props;
    return (
      <div className={styles.root}>
        {!readOnly && this.topButtons()}
        {useStatusProcessor ? (
          <DataStatusProcessor items={items} content={this.renderedItems} />
        ) : (
          this.renderedItems
        )}

        {this.paginator}
      </div>
    );
  }
}

TableView.propTypes = {
  history: PropTypes.shape().isRequired,
  items: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.arrayOf(PropTypes.shape()),
  ]).isRequired,
  readOnly: PropTypes.bool,
  location: PropTypes.shape().isRequired,
  useStatusProcessor: PropTypes.bool,
  itemsPerPage: PropTypes.number,
  map: PropTypes.arrayOf(PropTypes.shape()),
  linkPrefix: PropTypes.string,
  refresh: PropTypes.func,
  delete: PropTypes.func,
};

TableView.defaultProps = {
  useStatusProcessor: false,
  itemsPerPage: 10,
  map: null,
  linkPrefix: '',
  refresh: null,
  readOnly: false,
  delete: null,
};

export default withRouter(TableView);
