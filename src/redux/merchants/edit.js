import ApiController from '../../libs/api/controller';

import { METHODS } from '../../libs/api/constants';

import prefixes from '../prefixes';

const config = {
  prefix: prefixes.merchants,
  url: 'merchants',
  method: METHODS.PUT,
};

const controller = new ApiController(config);

const { fetch } = controller;

export const editItem = (id, data, onSuccess) =>
  fetch({ urlPostfix: id, body: data, onSuccess });

export default controller.reducer;
