import ApiController from '../../libs/api/controller';
import { METHODS } from '../../libs/api/constants';

import prefixes from '../prefixes';

const config = {
  prefix: prefixes.merchants,
  url: 'merchants',
  method: METHODS.POST,
};

const controller = new ApiController(config);

const { fetch } = controller;

export const addItem = (data, onSuccess) => fetch({ body: data, onSuccess });

export default controller.reducer;
