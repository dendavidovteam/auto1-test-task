import ApiController from '../../libs/api/controller';
import { METHODS } from '../../libs/api/constants';

import prefixes from '../prefixes';

const config = {
  prefix: prefixes.merchant,
  url: 'merchants',
  method: METHODS.GET,
  initialData: {
    bids: [],
  },
};

const controller = new ApiController(config);

const { fetch } = controller;

export const getItem = itemId => fetch({ urlPostfix: itemId });

export default controller.reducer;
