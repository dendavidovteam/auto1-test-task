import ApiController from '../../libs/api/controller';
import { METHODS } from '../../libs/api/constants';

import prefixes from '../prefixes';

const config = {
  prefix: prefixes.merchants,
  url: 'merchants',
  method: METHODS.GET,
};

const controller = new ApiController(config);

const { fetch } = controller;

export const getList = () => fetch();

export default controller.reducer;
