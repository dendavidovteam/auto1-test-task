import ApiController from '../../libs/api/controller';
import { METHODS } from '../../libs/api/constants';

import prefixes from '../prefixes';

const config = {
  prefix: prefixes.merchants,
  url: 'merchants',
  method: METHODS.DELETE,
};

const controller = new ApiController(config);

const { fetch } = controller;

export const deleteItems = (items, onSuccess) =>
  fetch({ body: items, onSuccess });

export default controller.reducer;
