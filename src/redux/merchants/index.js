import { combineReducers } from 'redux';

import listReducer, { getList } from './get-list';
import itemReducer, { getItem } from './get-item';
import addReducer, { addItem } from './add';
import editReducer, { editItem } from './edit';
import deleteReducer, { deleteItems } from './delete';

export const actions = {
  getList,
  getItem,
  editItem,
  addItem,
  deleteItems,
};

export default combineReducers({
  list: listReducer,
  item: itemReducer,
  add: addReducer,
  edit: editReducer,
  delete: deleteReducer,
});
