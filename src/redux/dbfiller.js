import ApiController from '../libs/api/controller';
import { METHODS } from '../libs/api/constants';

import prefixes from './prefixes';

const config = {
  prefix: prefixes.dbfiller,
  url: 'dbfiller',
  method: METHODS.POST,
};

const controller = new ApiController(config);

export const resetDb = controller.fetch;

export default controller.reducer;
