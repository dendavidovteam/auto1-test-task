import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import merchants from './merchants';
import dbfiller from './dbfiller';

export default combineReducers({
  form: formReducer,
  merchants,
  dbfiller,
});
