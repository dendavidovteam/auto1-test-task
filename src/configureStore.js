import { applyMiddleware, createStore, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import apiMiddleware from './libs/api/api-middleware';
import reducers from './redux/reducers';

const configureStore = preloadedState => {
  const enhancers = [];

  const middlewares = [thunkMiddleware, apiMiddleware];

  if (process.env.NODE_ENV === 'development') {
    const { devToolsExtension } = window;

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  return createStore(
    reducers,
    preloadedState,
    compose(
      applyMiddleware(...middlewares),
      ...enhancers
    )
  );
};

export default configureStore;
