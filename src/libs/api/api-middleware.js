import axios from 'axios';

import { REQUEST_STATUSES, METHODS } from './constants';

const apiMiddleware = ({ dispatch }) => next => action => {
  if (action.type !== 'API') {
    return next(action);
  }

  const {
    prefix,
    method = METHODS.GET,
    url,
    body = null,
    headers = {},
    urlPostfix,
    dataProcessor,
    onSuccess,
    ...axiosParams
  } = action.payload;

  if (typeof prefix !== 'string' || prefix.length === 0)
    throw new Error('payload.prefix should be string and have length > 0');

  if (typeof url !== 'string' || url.length === 0)
    throw new Error('payload.url should be string and have length > 0');

  const { REQUEST, SUCCESS, ERROR } = REQUEST_STATUSES;

  dispatch({
    type: `${prefix}_${method}_${REQUEST}`,
    payload: {
      status: REQUEST,
    },
  });
  return axios({
    method,
    url: `${process.env.API_URL}${urlPostfix ? `${url}/${urlPostfix}` : url}`,
    data: body,
    headers,
    ...axiosParams,
  })
    .then(({ data: { data } }) => {
      const payload =
        typeof dataProcessor === 'function' ? dataProcessor(data) : data;

      dispatch({
        type: `${prefix}_${method}_${SUCCESS}`,
        payload,
      });
      if (typeof onSuccess === 'function') onSuccess();
    })
    .catch(error => {
      dispatch({
        type: `${prefix}_${method}_${ERROR}`,
        payload: error.message,
      });
    });
};

export default apiMiddleware;
