import { getSearchParam } from './search';

describe('getSearchParam', () => {
  it('returns right value', () => {
    const value = getSearchParam('?page=2', 'page');
    expect(value).toEqual('2');
  });

  it('returns null', () => {
    const value = getSearchParam('', 'page');
    expect(value).toBeNull();
  });

  it('returns bar', () => {
    const value = getSearchParam('?page=2&foo=bar', 'foo');
    expect(value).toEqual('bar');
  });

  it('returns null for unknown property', () => {
    const value = getSearchParam('?page=2&foo=bar', 'test');
    expect(value).toBeNull();
  });
});
