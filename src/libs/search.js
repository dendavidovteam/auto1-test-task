export const searchInTree = (root, propertyId, id, propertyChildren) => {
  if (!Array.isArray(root)) return null;
  for (let i = 0; i < root.length; i += 1) {
    if (root[i][propertyId] === id) return root[i];

    const children = root[i][propertyChildren];
    if (children && Array.isArray(children)) {
      const result = searchInTree(children, propertyId, id, propertyChildren);
      if (result) return result;
    }
  }

  return null;
};

export const getSearchParam = (search, paramName) => {
  if (
    !search ||
    typeof search !== 'string' ||
    search[0] !== '?' ||
    search.length < 2 ||
    !paramName
  )
    return null;

  const pair = search
    .substring(1)
    .split('&')
    .find(item => item.split('=')[0] === paramName);

  return pair ? decodeURIComponent(pair.split('=')[1]) : null;
};

export const getPageFromSearchParam = search => {
  if (!search) return 0;
  const page = +getSearchParam(search, 'page');
  if (!page || typeof page !== 'number') return 0;
  return page - 1;
};
